---
title: "Od TGM k Zemanovi: Poslechněte si vánoční a novoroční projevy všech prezidentů"
perex: ""
description: "Českému rozhlasu se podařilo shromáždit ojedinělou kolekci vánočních a novoročních projevů všech jedenácti československých a českých prezidentů (počítáme mezi ně i státního prezidenta Protektorátu Čechy a Morava Emila Háchu). V naší aplikaci najdete kompletní texty projevů a u většiny z nich také zvukový záznam. Můžete si přehrát buď celý projev od začátku do konce, nebo kliknutím na červeně podtržené části textu jen vybrané úryvky."
authors: ["Jan Boček", "Jan Cibulka", "Petr Kočí"]
published: "26. prosince 2017"
socialimg: https://interaktivni.rozhlas.cz/data/prezidentske-projevy-2017/www/media/social3.png
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "prezidentske-projevy"
libraries: [d3]
---

