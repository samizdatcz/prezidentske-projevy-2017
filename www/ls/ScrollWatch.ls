class ig.ScrollWatch
  (@projevSelector, @leftArrow, @rightArrow, @player) ->
    @leftArrowElm = @leftArrow.node!
    @rightArrowElm = @rightArrow.node!
    @eleNode = @projevSelector.element.node!
    @playerNode = @player.element.node!
    @projevOffset = ig.utils.offset @projevSelector.parentElement.node!
    window.addEventListener \resize @onResize
    document.addEventListener \scroll @onScroll

  onResize: ~>
    @projevOffset := ig.utils.offset @projevSelector.parentElement.node!
    @onScroll!

  onScroll: ~>
    @onScrollProjev!
    @onScrollArrows!
    @onScrollPlayer!

  onScrollPlayer: ->
    scroll = (window.document.documentElement.scrollTop || window.document.body.scrollTop) - 152px
    if scroll > 0
      @playerNode.style
        ..position = 'fixed'
        ..top = '202px'
        ..left = "#{@projevOffset.left + 733}px"
    else
      @playerNode.style
        ..position = 'absolute'
        ..top = '200px'
        ..left = "auto"

  onScrollArrows: ->
    scroll = (window.document.documentElement.scrollTop || window.document.body.scrollTop) - 35px
    if scroll > 0
      @leftArrowElm
        ..className = "arrow left fixed"
        ..style.left = "#{@projevOffset.left}px"
      @rightArrowElm
        ..className = "arrow right fixed"
        ..style.left = "#{@projevOffset.left + 940}px"
    else
      @leftArrowElm
        ..className = "arrow left"
        ..style.left = ""
      @rightArrowElm
        ..className = "arrow right"
        ..style.left = ""

  onScrollProjev: ->
    scroll = (window.document.documentElement.scrollTop || window.document.body.scrollTop) - 0px
    if scroll > 0
      @eleNode.style
        ..position = "fixed"
        ..top = "45px"
        ..left = "#{@projevOffset.left + 18}px"
      @eleNode.className = "projev-selector"
    else
      @eleNode.style
        ..position = "absolute"
        ..top = "-9px"
        ..left = "18px"
      @eleNode.className = "projev-selector top"
