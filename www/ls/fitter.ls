for el in document.querySelectorAll "a[href^='http:'],a[href^='https:']"
  continue if el.id is "logo"
  continue if el.classList?contains "share"
  el.addEventListener \click (evt) ->
    console.log \click
    evt.preventDefault!
    window.open @getAttribute \href

for el in document.querySelectorAll "a.share"
  el.addEventListener \click (evt) ->
    evt.preventDefault!
    window.open do
      @getAttribute \href
      ''
      "width=550,height=265"

isAtTop = yes
header = document.querySelector "body > header"
isWithoutTopImage = yes

setHeaderClass = ->
  top = (document.body.scrollTop || document.documentElement.scrollTop)
  triggerHeight = if isWithoutTopImage then 1 else window.innerHeight
  if top >= triggerHeight and isAtTop
    header.classList?remove \at-top
    isAtTop := no
  else if top < triggerHeight and not isAtTop
    header.classList?add \at-top
    isAtTop := yes

window.addEventListener \scroll setHeaderClass
setHeaderClass!
